# development mode
# it automatically opens `http://localhost:8080` in your default browser,

$ yarn dev

# check types
$ yarn check-types

# production build
$ yarn build

# production mode
$ yarn start
```
