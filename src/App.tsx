import React from 'react';
import './index.scss';
import SideBar from './components/sidebar/index';

const App = () => {
      return (
            <>
                <SideBar pageWrapId={'page-wrap'} outerContainerId={'app'} />
                <div id='page-wrap'>
                    <div className='flex-main-container'>
                        <header>hola</header>
                        <section>section</section>
                    </div>
                </div>
            </>
      );
};

export default App;
